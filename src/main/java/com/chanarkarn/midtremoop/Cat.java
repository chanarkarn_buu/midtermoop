/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.midtremoop;

/**
 *
 * @author A_R_T
 */
public class Cat {

    private String name;
    String gender;
    Float weight;
    String hobby;
    Boolean isInRelationShip;

    public Cat(String name, String gender, Float weight, String hobby) {

        this.name = name;
        this.gender = gender;
        this.weight = weight;
        this.hobby = hobby;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
    
    public void looking() {
        System.out.println("Name: " + name + " Gender:" + gender + " Weight:" + weight + " kg.");

    }
   
}
