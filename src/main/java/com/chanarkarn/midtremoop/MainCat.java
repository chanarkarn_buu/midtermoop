/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.midtremoop;

/**
 *
 * @author A_R_T
 */
public class MainCat extends Cat {
    protected String name;
    protected String hobby;
    protected String gender;
 

    public MainCat(String name, String gender, Float weight, String hobby) {
        super(name, gender, weight, hobby);
        System.out.println("Cat created");
        this.name = name;
        this.gender = gender;
        this.weight = weight;
        this.hobby = hobby;

    }

    public void looking() {
        System.out.println("name: " + this.name + " gender: " + this.gender
                + " weight: " + this.weight + " hobby: " + this.hobby);
        System.out.println("Cat looking at birds in the morning.");
    }

    public void hunting() {
        System.out.println("Cat hunting black rats.");

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public Float getWeight() {
        return weight;
    }

    @Override
    public String getHobby() {
        return hobby;
    }
}
