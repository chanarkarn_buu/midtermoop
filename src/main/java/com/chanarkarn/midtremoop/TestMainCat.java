/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.midtremoop;

/**
 *
 * @author A_R_T
 */
public class TestMainCat {

    public static void main(String[] args) {
        // write your code here
        MainCat maincat = new MainCat("Monty", "Male", 8.0f, "run");
        maincat.looking();
        maincat.hunting();

        Cat jenny = new Cat("Jenny", "Female", 7.0f, "play ball");
        maincat.looking();
        maincat.hunting();
        
        Cat kelvin = new Cat("Kelvin", "Female", 8.5f, "play ball and run");
        maincat.looking();
        maincat.hunting();

 
    }
}
